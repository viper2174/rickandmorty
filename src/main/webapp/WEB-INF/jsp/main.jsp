<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<html lang="en">
<body>
<a href="/rickmorty/showRandomCharacter">Show random Rick&Morty character</a></br>
<h1>Show Rick&Morty characters whose name contains specified symbols</h1>
<form action="/rickmorty/showCharactersByNameFilter" modelAttribute="filterForm">
   <input type="text" name="name"></input>
   <input type="submit" value="Search"></input>
</form>
</body>
</html>