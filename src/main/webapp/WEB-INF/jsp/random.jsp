<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html lang="en">
<body>
Random Rick&Morty character
<c:if test="${not empty character}">
      <table>
          <tr>
              <td>Name:</td>
              <td>${character.name}</td>
          </tr>
          <tr>
              <td>Origin:</td>
              <td>${character.origin.name}</td>
          </tr>
          <tr>
              <td>Location:</td>
              <td>${character.location.url}</td>
          </tr>
      </table>
</c:if>
</body>
</html>