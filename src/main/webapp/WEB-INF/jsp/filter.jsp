<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<html lang="en">
<body>
<c:if test="${not empty characters}">
      <table>
          <thead>
             <tr>
                 <th>Id</th>
                 <th>Name</th>
                 <th>Gender</th>
                 <th>Status</th>
                 <th>Location</th>
                 <th>Origin</th>
             </tr>
          </thead>
          <tbody>
          <c:forEach var="character" items="${characters}">
              <tr>
                  <th>${character.id}</th>
                  <th>${character.name}</th>
                  <th>${character.gender}</th>
                  <th>${character.status}</th>
                  <th>${character.location.name}</th>
                  <th>${character.origin.name}</th>
              </tr>
          </c:forEach>
          </tbody>
      </table>
</c:if>
<c:if test="${empty characters}">
      <h1>There are no Rick&Morty Character with specified name
</c:if>
</body>
</html>