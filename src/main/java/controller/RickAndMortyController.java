package controller;

import api.RickAndMortyApi;
import entity.dbEntities.Character;
import forms.FilterForm;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@Api(value = "Rick&Morty Api", description = "Api is providing information about Rick&Morty characters")
public class RickAndMortyController{

    private static final String RICK_AND_MORTY_MAIN_PAGE = "main";
    private static final String RANDOM_CHARACTER_PAGE = "random";
    private static final String FILTER_CHARACTER_NAME_PAGE = "filter";

    @Autowired
    private RickAndMortyApi api;

    @GetMapping("/")
    private ModelAndView getRickAndMortyMainPage() {
        ModelAndView modelAndView = new ModelAndView(RICK_AND_MORTY_MAIN_PAGE);
        modelAndView.addObject("filterForm", new FilterForm());
        modelAndView.addObject("filterFormForJson", new FilterForm());
        return modelAndView;
    }

    @GetMapping("/showRandomCharacter")
    @ApiOperation(value = "View the information about random Rick&Morty character", response = ModelAndView.class)
    private ModelAndView showInfoAboutRandomCharacter(Model model) {
        Character character = api.getRandomCharacter();
        ModelAndView modelAndView = new ModelAndView(RANDOM_CHARACTER_PAGE);
        modelAndView.addObject("character", character);
        return modelAndView;
    }

    @GetMapping("/showCharactersByNameFilter")
    @ApiOperation(value = "View the information about Rick&Morty characters, who filled name filter",
            response = ModelAndView.class)
    private ModelAndView showFilteredCharacters
            (@ApiParam (value = "String object containing name filter", required = true)
             @ModelAttribute("filterForm") FilterForm filterForm) {
        List<Character> characterList = api.getCharactersByNameFilter(filterForm.getName());
        ModelAndView modelAndView = new ModelAndView(FILTER_CHARACTER_NAME_PAGE);
        modelAndView.addObject("characters", characterList);
        return modelAndView;
    }
}
