package repositories;

import entity.dbEntities.Character;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CharacterRepository extends JpaRepository<Character, Long> {

    List<Character> findByNameIgnoreCaseContaining(String name);

    @Query("SELECT c FROM Character c ORDER BY random()")
    List<Character> findRandomCharacter (Pageable pageable);

}
