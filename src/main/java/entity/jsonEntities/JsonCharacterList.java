package entity.jsonEntities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class JsonCharacterList {

    private List<JsonCharacter> results;

    public JsonCharacterList() {
        results = new ArrayList<>();
    }

    public List<JsonCharacter> getResults() {
        return results;
    }

    public void setResults(List<JsonCharacter> results) {
        this.results = results;
    }

    @Override
    public String toString() {
        return "JsonCharacterList{" +
                "results=" + results +
                '}';
    }
}
