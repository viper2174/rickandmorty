package cronjob;

import entity.jsonEntities.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import services.CharacterService;

import java.util.ArrayList;
import java.util.List;

@Component
public class DatabaseUpdateCronJob {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CharacterService characterService;

    // This cron expression means that updateDatabase() method will execute twice a hour
    @Scheduled(cron = "0 0/30 * * * ?")
    @Transactional
    public void updateDatabase (){
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<InfoObject> response = restTemplate.exchange(
                "https://rickandmortyapi.com/api/character/",
                HttpMethod.GET, null, new ParameterizedTypeReference<InfoObject>(){});
        Info info = response.getBody().getInfo();
        int numberOfPages = info.getPages();
        List<JsonCharacter> allJsonCharacters = new ArrayList<>();
        logger.info ("obtaining json info from rick&morty API");
        for (int i = 1; i <= numberOfPages; i++) {
            JsonCharacterList jsonCharacterList = restTemplate.getForObject
                    (String.format("https://rickandmortyapi.com/api/character/?page=%s", i), JsonCharacterList.class);
            List<JsonCharacter> queryJsonCharacters = jsonCharacterList.getResults();
            allJsonCharacters.addAll(queryJsonCharacters);
        }
        logger.info("json info has been obtained");
        logger.info("adding json info into local database");
        characterService.updateCharacterDatabase (allJsonCharacters);
        logger.info("local database has been updated");
    }

}
