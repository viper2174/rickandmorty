package services;

import entity.dbEntities.Character;
import entity.jsonEntities.JsonCharacter;

import java.util.List;

public interface CharacterService {

    List<Character> getCharactersByFilterName(String name);
    void updateCharacterDatabase(List<JsonCharacter> allJsonCharacters);
    Character findRandomCharacter ();
}
