package services;

import entity.dbEntities.Character;
import entity.dbEntities.Location;
import entity.dbEntities.Origin;
import entity.jsonEntities.JsonCharacter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import repositories.CharacterRepository;
import repositories.LocationRepository;
import repositories.OriginRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class CharacterServiceImpl implements CharacterService{

    private static final int FIRST_PAGE = 0;
    private static final int LIMITED_BY_ONE = 1;
    @Autowired
    private CharacterRepository characterRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private OriginRepository originRepository;

    @Override
    public List<Character> getCharactersByFilterName(String name) {
        return characterRepository.findByNameIgnoreCaseContaining(name);
    }

    @Override
    @Transactional
    public void updateCharacterDatabase(List<JsonCharacter> allJsonCharacters) {
        characterRepository.deleteAll();
        originRepository.deleteAll();
        locationRepository.deleteAll();
        List<Character> characters = new ArrayList<>();
        List<Origin> origins = new ArrayList<>();
        List<Location> locations = new ArrayList<>();
        for (JsonCharacter ch: allJsonCharacters){
            Origin origin = getOriginFromJsonInfo(ch);
            Location location = getLocationFromJsonInfo(ch);
            Character character = getCharacterFromJsonInfo(ch, origin, location);
            characters.add(character);
            origins.add(origin);
            locations.add(location);
        }
        originRepository.saveAll(origins);
        locationRepository.saveAll(locations);
        characterRepository.saveAll(characters);
    }

    private Origin getOriginFromJsonInfo(JsonCharacter ch) {
        Origin origin = new Origin();
        origin.setName(ch.getOrigin().getName());
        origin.setUrl(ch.getOrigin().getUrl());
        return origin;
    }

    private Location getLocationFromJsonInfo(JsonCharacter ch) {
        Location location = new Location();
        location.setName(ch.getLocation().getName());
        location.setUrl(ch.getLocation().getUrl());
        return location;
    }

    private Character getCharacterFromJsonInfo
            (JsonCharacter ch, Origin origin, Location location) {
        Character character = new Character();
        character.setId(ch.getId());
        character.setName(ch.getName());
        character.setGender(ch.getGender());
        character.setCreated(ch.getCreated());
        character.setImage(ch.getImage());
        character.setSpecies(ch.getSpecies());
        character.setStatus(ch.getStatus());
        character.setUrl(ch.getUrl());
        character.setType(ch.getType());
        character.setOrigin(origin);
        character.setLocation(location);
        return character;
    }

    @Override
    public Character findRandomCharacter() {
        List<Character> characters =
                characterRepository.findRandomCharacter(PageRequest.of(FIRST_PAGE,LIMITED_BY_ONE));
        return characters.get(0);
    }
}