package api;

import entity.dbEntities.Character;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import services.CharacterService;

import java.util.List;

@Component
public class RickAndMortyPostgreSqlApi implements RickAndMortyApi {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private CharacterService characterService;

    @Override
    public Character getRandomCharacter() {
        return characterService.findRandomCharacter();
    }

    @Override
    public List<Character> getCharactersByNameFilter (String name) {
        return characterService.getCharactersByFilterName(name);
    }
}
