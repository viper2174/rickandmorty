package api;

import entity.dbEntities.Character;

import java.util.List;

public interface RickAndMortyApi {

    Character getRandomCharacter();
    List<Character> getCharactersByNameFilter(String name);

}
