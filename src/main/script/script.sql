-- I've generated this script using PostgreSQL pgAdmin4 backup tool --
DROP DATABASE rickmorty;
CREATE DATABASE rickmorty WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Russian_Russia.1251' LC_CTYPE = 'Russian_Russia.1251';
ALTER DATABASE rickmorty OWNER TO test;

\connect rickmorty

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


CREATE TABLE public."character" (
    id integer NOT NULL,
    name character varying(100),
    status character varying(100),
    species character varying(100),
    type character varying(100),
    gender character varying(100),
    image character varying(100),
    url character varying(100),
    origin_id integer NOT NULL,
    location_id integer NOT NULL,
    created character varying(100)
);

CREATE SEQUENCE public.character_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.character_id_seq OWNED BY public."character".id;

CREATE TABLE public.location (
    id integer NOT NULL,
    name character varying(100),
    url character varying(100)
);

CREATE SEQUENCE public.location_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.location_id_seq OWNED BY public.location.id;


CREATE TABLE public.origin (
    id integer NOT NULL,
    name character varying(100),
    url character varying(100)
);


CREATE SEQUENCE public.origin_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

ALTER SEQUENCE public.origin_id_seq OWNED BY public.origin.id;

ALTER TABLE ONLY public."character" ALTER COLUMN id SET DEFAULT nextval('public.character_id_seq'::regclass);
ALTER TABLE ONLY public.location ALTER COLUMN id SET DEFAULT nextval('public.location_id_seq'::regclass);
ALTER TABLE ONLY public.origin ALTER COLUMN id SET DEFAULT nextval('public.origin_id_seq'::regclass);

ALTER TABLE ONLY public."character"
    ADD CONSTRAINT character_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.location
    ADD CONSTRAINT location_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public.origin
    ADD CONSTRAINT origin_pkey PRIMARY KEY (id);

ALTER TABLE ONLY public."character"
    ADD CONSTRAINT character_location_id_fkey FOREIGN KEY (location_id) REFERENCES public.location(id);


ALTER TABLE ONLY public."character"
    ADD CONSTRAINT character_origin_id_fkey FOREIGN KEY (origin_id) REFERENCES public.origin(id);

GRANT ALL ON SCHEMA public TO PUBLIC;


ALTER DEFAULT PRIVILEGES FOR ROLE postgres GRANT ALL ON TABLES  TO test;

